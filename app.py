from flask import Flask, render_template, jsonify, request
from flask_cors import CORS
import json, hashlib
# import mysql.connector
# cnx = mysql.connector.connect(user='root', passwd="root", database='mydb', host='192.168.99.100') #, auth_plugin='root')
import hashlib
import binascii
import os
import requests
import urllib.request

app = Flask(__name__)
CORS(app, supports_credentials=True)

portfolio_data = None

data = [ {"instrumentName": "Floral", "cpty": "Selvyn", "price": 400.03286275376837, "type": "S", "quantity": 25, "time": "14-Aug-2019 (11:41:03.800205)"},
    {"instrumentName": "Borealis", "cpty": "John", "price": 5711.70929526531, "type": "S", "quantity": 1, "time": "14-Aug-2019 (11:41:03.825817)"},
    {"instrumentName": "Heliosphere", "cpty": "Nidia", "price": 7743.821020968979, "type": "S", "quantity": 26, "time": "14-Aug-2019 (11:41:04.002633)"},
    {"instrumentName": "Jupiter", "cpty": "Selvyn", "price": 3516.741711739396, "type": "S", "quantity": 238, "time": "14-Aug-2019 (11:41:04.250617)"},
    {"instrumentName": "Koronis", "cpty": "Selvyn", "price": 2489.0898868346435, "type": "B", "quantity": 5, "time": "14-Aug-2019 (11:41:04.548710)"},
    {"instrumentName": "Floral", "cpty": "Nidia", "price": 401.66141623662793, "type":
    "B", "quantity": 5, "time": "14-Aug-2019 (11:41:04.575111)"},
    {"instrumentName": "Deuteronic", "cpty": "John", "price": 8530.524721086578, "type": "B", "quantity": 10, "time": "14-Aug-2019 (11:41:04.632653)"},
    {"instrumentName": "Interstella", "cpty": "John", "price": 3561.3457361923984, "type": "B", "quantity": 51, "time": "14-Aug-2019 (11:41:04.651014)"},
    {"instrumentName": "Interstella", "cpty": "Lina", "price": 3491.4128360636482, "type": "S", "quantity": 103, "time": "14-Aug-2019 (11:41:04.771238)"},
    {"instrumentName": "Jupiter", "cpty": "Selvyn", "price": 3515.858790581119, "type": "S", "quantity": 4, "time": "14-Aug-2019 (11:41:04.794712)"},
    {"instrumentName": "Galactia", "cpty": "Richard", "price": 10166.738306494135, "type": "B", "quantity": 3, "time": "14-Aug-2019 (11:41:04.994841)"},
    {"instrumentName": "Deuteronic", "cpty": "Nidia", "price": 8367.03545152154, "type": "S", "quantity": 996, "time": "14-Aug-2019 (11:41:05.277909)"},
    {"instrumentName": "Heliosphere", "cpty": "Richard", "price": 7879.787983613376, "type": "B", "quantity": 191, "time": "14-Aug-2019 (11:41:05.397054)"},
    {"instrumentName": "Koronis", "cpty": "Lewis", "price": 2489.681718270678, "type":
    "B", "quantity": 327, "time": "14-Aug-2019 (11:41:05.559428)"}
]

@app.route("/")
def hello():
    return render_template('deals.html')

@app.route("/generateData", methods=['GET'])
def generate_data():
    data = request.get_json()
    print(data)


def instrument(price_times_shares, total_number_of_shares, realised, effective, inst):
    return {
        # price * shares ... + price * shares....
        'price_times_shares': price_times_shares,
        'total_number_of_shares': total_number_of_shares,  # ongoing total
        'weighted': price_times_shares / total_number_of_shares,
        'realised': realised,
        'effective': effective,
        'inst': inst
    }

@app.route("/deals", methods=['GET'])
def deals():
    host = 'dao-server-14.react-master.svc.cluster.local'
    port = '8080'
    url = 'http://{0}:{1}/selectData'.format(host, port)
    data = urllib.request.urlopen(url).read().decode('utf-8')
    return jsonify(data)

@app.route("/portfolio", methods=['GET'])
def portfolio():
    portfolio = {}

    host = 'dao-server-14.react-master.svc.cluster.local'
    port = '8080'
    url = 'http://{0}:{1}/selectData'.format(host, port)
    data = urllib.request.urlopen(url).read().decode('utf-8')
    data = json.loads(data)
    for item in data:
        print(item)
        item = json.loads(item)
        inst = item['instrumentName']
        price = item['price']
        shares = item['quantity']
        pts = price * shares
        if inst not in portfolio:
            if item['type'] == 'B':
                portfolio[inst] = instrument(pts, shares, 0, pts, inst)
            else:
                portfolio[inst] = instrument(0 - pts, shares, pts, 0, inst)
        else:
            num = portfolio[inst]['price_times_shares']
            den = portfolio[inst]['total_number_of_shares']
            realised = portfolio[inst]['realised']
            effective = portfolio[inst]['effective']

            if item['type'] == 'B':
                portfolio[inst] = instrument(num + pts, shares + den, realised, effective + pts, inst)
            else:
                portfolio[inst] = instrument(num - pts, shares - den, realised + pts, effective, inst)

    arr = []

    for key, val in portfolio.items():
        del portfolio[key]['price_times_shares']
        arr.append(portfolio[key])

    portfolio_data = portfolio

    return jsonify(arr)

def verify_password(stored_password, provided_password):
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha256',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password


@app.route("/submitForm", methods=['POST'])
def submitForm():
    username = request.get_json()['username']
    password = request.get_json()['password']
    data = requests.post(url="dao-server-14.react-master.svc.cluster.local/selectUser", data={'name': 'trader'}).json()
    return jsonify(data)
    # cursor = cnx.cursor()
    # query = "SELECT user_pw FROM Trader WHERE user_name = %s"
    # cursor.execute(query, (username, ))
    # msg = "success"
    #
    # return jsonify(message=msg, type_user=username)
    # login_info = requests.get(url="/asodnalsdnasldjnasjldn").json() # replace this with the GET request GIVEN username
    # type_user = login_info
    # pw = login_info # select password
    # if verify_password(pw, password):
    #     msg = "success"
    # # cursor.close()
    # return jsonify(message=msg, type_user=type_user)

@app.route("/senior_info", methods=['POST'])
def senior_trader():
    """
        Get list of traders and their positions
    """

# @app.route("/history", methods=['POST'])
# def senior_trader():
#     pass
# @app.route("/getuserinfo", methods=['GET'])
# def sync():

#     pass



@app.route("/sync", methods=['GET'])
def sync():
    msg = "Failed to connect to database"
    try:
        cnx = mysql.connector.connect(user='root', passwd="root", database='mydb', host='192.168.99.100') #, auth_plugin='root')
        cnx.close()
        msg = "Successfully connected to the database"
    except:
        pass
    return jsonify(message=msg)

def bootapp():
    app.run(port=8081, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
    bootapp()
    # cnx.close()
