
data = [
    {'instrumentName': 'Floral', 'cpty': 'Lina', 'price': 393.5646639757584,
        'type': 'S', 'quantity': 3, 'time': '11-Aug-2019 (14:58:40.025796)'},
    {'instrumentName': 'Floral', 'cpty': 'John', 'price': 401.61337518273825,
        'type': 'B', 'quantity': 972, 'time': '11-Aug-2019 (14:58:40.148110)'},
    {'instrumentName': 'Celestial', 'cpty': 'Lina', 'price': 1452.4043359813525,
        'type': 'B', 'quantity': 1, 'time': '11-Aug-2019 (14:58:40.291418)'},
    {'instrumentName': 'Koronis', 'cpty': 'Selvyn', 'price': 2443.020198764476,
        'type': 'S', 'quantity': 3, 'time': '11-Aug-2019 (14:58:40.491488)'},
    {'instrumentName': 'Galactia', 'cpty': 'Selvyn', 'price': 10165.98101740875,
        'type': 'B', 'quantity': 557, 'time': '11-Aug-2019 (14:58:40.511141)'}
]

portfolio = {}
# obj = {'Floral': {'numerator': 389187.50668569433, 'denominator': 975}, 'Celestial': {'numerator': 1452.4043359813525, 'denominator': 1},
#    'Koronis': {'numerator': -7329.060596293428, 'denominator': 3}, 'Galactia': {'numerator': 5662451.426696674, 'denominator': 557}}


def instrument(price_times_shares, total_number_of_shares, realised, effective):
    return {
        # price * shares ... + price * shares....
        'price_times_shares': price_times_shares,
        'total_number_of_shares': total_number_of_shares,  # ongoing total
        'weighted': price_times_shares / total_number_of_shares,
        'realised': realised,
        'effective': effective
    }


for item in data:
    inst = item['instrumentName']
    price = item['price']
    shares = item['quantity']
    pts = price * shares
    if inst not in portfolio:
        if item['type'] == 'B':
            portfolio[inst] = instrument(pts, shares, 0, pts)
        else:
            portfolio[inst] = instrument(0 - pts, shares, pts, 0)
    else:
        num = portfolio[inst]['price_times_shares']
        den = portfolio[inst]['total_number_of_shares']
        realised = portfolio[inst]['realised']
        effective = portfolio[inst]['effective']

        if item['type'] == 'B':
            portfolio[inst] = instrument(num + pts, shares + den, realised, effective + pts)
        else:
            portfolio[inst] = instrument(num - pts, shares - den, realised + pts, effective)

pnl = 0


print(portfolio)